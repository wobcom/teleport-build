.PHONY: deps build deploy

VERSION=4.1.1
DEPLOYLOCATION=ubuntu@static.iot.wolfsburg.digital:/var/www/static.iot.wolfsburg.digital/teleport/

TOOLS=teleport tctl tsh

define xgo_build
	xgo -v \
	-out $(1)-$(VERSION) \
	--targets=linux/arm-5,linux/arm-6,linux/arm-7,linux/amd64 \
	--branch $(VERSION) \
	--pkg tool/$(1)\
	--remote https://gitlab.com/wobcom/teleport.git github.com/gravitational/teleport;
endef

define scp
	scp $(1)-$(VERSION)-linux-* $(DEPLOYLOCATION);
endef

deps:
	go get -v github.com/karalabe/xgo

build:
	$(foreach tool,$(TOOLS),$(call xgo_build,$(tool)))

deploy:
	$(foreach tool,$(TOOLS),$(call scp,$(tool)))
